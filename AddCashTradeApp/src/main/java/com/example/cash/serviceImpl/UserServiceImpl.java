package com.example.cash.serviceImpl;



import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.cash.dto.AccountResponse;
import com.example.cash.dto.ApiResponse;
import com.example.cash.entity.Userdetails;
import com.example.cash.exception.AccountNotFoundException;
import com.example.cash.exception.InsufficientBalance;
import com.example.cash.feignclient.AccountFeignClient;
import com.example.cash.repository.UserRepository;
import com.example.cash.service.UserService;
import com.example.cash.utils.Response;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	
		private final UserRepository userrepo;
		private final AccountFeignClient accountFeignClient;

		@Override
		public ApiResponse addCash(Long userId, BigDecimal casbalance) {
			log.info("addCash method in AccountServiceImpl");
			Userdetails usr = userrepo.findById(userId).orElseThrow(() -> {
				log.error("user cannot be found");
				throw new AccountNotFoundException(Response.USER_NOT_FOUND_CODE,
						Response.USER_NOT_FOUND_MESSAGE);
			});
			AccountResponse account = accountFeignClient.viewAccounts(userId).getBody();
			if(account.apiResponse().httpcode() == Response.USER_NOT_FOUND_CODE) {
				throw new AccountNotFoundException(Response.USER_NOT_FOUND_CODE,Response.USER_NOT_FOUND_MESSAGE);
			}
			usr.setCashBalance(usr.getCashBalance().add(casbalance));


			if (account.accountRequest().openingBalance().compareTo(casbalance) >= 0) {
//				account.setOpeningBalance(account.getOpeningBalance().subtract(casbalance));
//				accountrepo.save(account);
			} else {
				log.error("acccount has insufficient balance");
				throw new InsufficientBalance(Response.INSUFFICIENT_BALANCE_IN_ACCOUNT_CODE,
						Response.INSUFFICIENT_BALANCE_IN_ACCOUNT);
			}
			userrepo.save(usr);
			return new ApiResponse(Response.ADD_CASH_CODE, Response.ADD_CASH_MESSAGE);
		}
}
