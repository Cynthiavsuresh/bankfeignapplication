package com.example.cash.dto;

public record ApiResponse(Long httpcode,String message) {

}
