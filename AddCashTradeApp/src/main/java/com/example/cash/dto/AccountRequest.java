package com.example.cash.dto;

import java.math.BigDecimal;

public record AccountRequest(BigDecimal openingBalance) {

}
