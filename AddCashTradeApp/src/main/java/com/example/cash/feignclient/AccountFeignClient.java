package com.example.cash.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.cash.dto.AccountResponse;
import com.example.cash.dto.ApiResponse;

@FeignClient(value="account",url="http://localhost:8081/account")
public interface AccountFeignClient {
	
	@GetMapping("/accounts/{userId}")
	public ResponseEntity<AccountResponse> viewAccounts(@PathVariable("userId") Long userId);
	

}
