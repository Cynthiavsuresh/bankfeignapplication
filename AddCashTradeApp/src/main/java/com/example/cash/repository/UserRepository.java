package com.example.cash.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.cash.entity.Userdetails;

public interface UserRepository extends JpaRepository<Userdetails, Long> {

}
