package com.example.cash.utils;

public interface Response {


	Long USER_NOT_FOUND_CODE=400l;
	String USER_NOT_FOUND_MESSAGE="User not found";
	String NO_RECORD_FOUND_MESSAGE="no record found";
	Long NO_RECORD_FOUND_CODE=404l;
	String SUCCESS_STATUS_MESSAGE="CurrentBalanc in the account";
	Long SUCCESS_STATUS_CODE=200l;
	Long INSUFFICIENT_BALANCE_IN_ACCOUNT_CODE=116l;
	String INSUFFICIENT_BALANCE_IN_ACCOUNT="issufficient balance in account";
	Long ADD_CASH_CODE=200l;
	String ADD_CASH_MESSAGE="Successful";
}
