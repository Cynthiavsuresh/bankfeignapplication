package com.example.cash.controller;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cash.dto.ApiResponse;
import com.example.cash.service.UserService;

import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
	

	private final UserService userservice;

	/**
	 * 
	 * @param userId      is passed as the path variable
	 * @param cashbalance is amount to be added to the trade app
	 * @return
	 */
	@PutMapping("/{userId}")
	public ResponseEntity<ApiResponse> addCash(@Min(value = 1) @PathVariable Long userId,
			@Min(value = 1) @RequestParam BigDecimal cashbalance) {
		log.info("addCash method in UserController");
		// Rsponsedto response = userservice.addCash(userId, cashbalance);
		return new ResponseEntity<>(userservice.addCash(userId, cashbalance), HttpStatus.OK);
	}

}
