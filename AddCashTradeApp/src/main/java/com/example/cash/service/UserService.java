package com.example.cash.service;

import java.math.BigDecimal;

import com.example.cash.dto.ApiResponse;

public interface UserService {

	ApiResponse addCash( Long userId, BigDecimal casbalance);

}
