package com.example.account.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.account.dto.AccountResponse;
import com.example.account.service.AccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j 
@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {
	
	private final AccountService accountService;
	
	@GetMapping("/{accountId}")
	public ResponseEntity<AccountResponse> viewAccounts( @PathVariable("accountId") Long accountId)
	{
		log.info("get rhe openbalance from the account");
		
		AccountResponse response=  accountService.viewAccount(accountId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}}
