package com.example.account.utils;

public interface Response {


	Long USER_NOT_FOUND_CODE=400l;
	String USER_NOT_FOUND_MESSAGE="User not found";
	String NO_RECORD_FOUND_MESSAGE="no record found";
	Long NO_RECORD_FOUND_CODE=404l;
	String SUCCESS_STATUS_MESSAGE="CurrentBalanc in the account";
	Long SUCCESS_STATUS_CODE=200l;
}
