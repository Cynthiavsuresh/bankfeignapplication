package com.example.account.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long accountId;
	private BigDecimal openingBalance;
	private Long accountNo;
	private String IFSCcode;
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name="userId")
	private Long userId;
}