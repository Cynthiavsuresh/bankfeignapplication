package com.example.account.serviceImpl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.account.dto.AccountRequest;
import com.example.account.dto.AccountResponse;
import com.example.account.dto.ApiResponse;
import com.example.account.entity.Account;
import com.example.account.repository.AccountRepository;
import com.example.account.service.AccountService;
import com.example.account.utils.Response;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImp implements AccountService {
	
	private final AccountRepository accountRepository;

	@Override
	public AccountResponse viewAccount(Long userId) {
		
		Optional<Account> account = accountRepository.findById(userId);
		if(!account.isPresent()) {
			return new AccountResponse (new ApiResponse(Response.USER_NOT_FOUND_CODE, Response.USER_NOT_FOUND_MESSAGE), new AccountRequest(BigDecimal.valueOf(0.0)));
 
		}
				
//		
//		log.info("RentalServiceImpl - toViewListOfrental details");
//		Pageable pageable = PageRequest.of(page, size);
//		Page<Account> rentalList = accountRepository.findAll(pageable);
//		
//		if (rentalList.isEmpty()) {
//			log.warn(Response.NO_RECORD_FOUND_MESSAGE);
//			throw new AccoungtDetailsException(Response.NO_RECORD_FOUND_CODE,Response.NO_RECORD_FOUND_MESSAGE);
//		}
		
		AccountRequest accountRequest=new AccountRequest(account.get().getOpeningBalance());
			
		log.info(Response.SUCCESS_STATUS_MESSAGE);
		return new AccountResponse (new ApiResponse(Response.SUCCESS_STATUS_CODE, Response.SUCCESS_STATUS_MESSAGE),accountRequest);
	}

}
