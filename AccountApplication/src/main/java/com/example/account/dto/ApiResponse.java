package com.example.account.dto;

public record ApiResponse(Long httpcode,String message) {

}
