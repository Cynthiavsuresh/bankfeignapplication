package com.example.account.dto;

public record AccountResponse(ApiResponse apiResponse,AccountRequest accountRequest) {

}
