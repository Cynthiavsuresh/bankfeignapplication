package com.example.account.dto;

import java.math.BigDecimal;

public record AccountRequest(BigDecimal openingBalance) {

}
