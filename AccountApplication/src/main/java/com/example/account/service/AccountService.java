package com.example.account.service;

import com.example.account.dto.AccountResponse;

public interface AccountService {

	AccountResponse viewAccount(Long userId);

}
